# RGBa Tools

## Work In Progress

### Blender addon
- asset export and scene edition for gamedev

### Licence
- Blender Python code is available under the terms of GPL3 licence (https://www.gnu.org/licenses/gpl-3.0.html)
- Godot code is available under the terms of MIT (expat) licence (https://opensource.org/licenses/MIT​)

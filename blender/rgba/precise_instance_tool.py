"""Precise Instance Tool """

from math import atan2
import bpy
from bpy.types import Operator, WorkSpaceTool, SpaceView3D
from bpy.props import BoolProperty
from bpy_extras import view3d_utils
from mathutils import Matrix, Vector
from .utils import add_prop_instance, get_ray_cast_result, \
    set_prop_loc_rot
from .draw import draw_callback_px


class PreciseInstanceTool(WorkSpaceTool):
    """Instance a collection at cursor position"""
    bl_space_type = 'VIEW_3D'
    bl_context_mode = 'OBJECT'
    bl_idname = "rgba.precise_instance"
    bl_label = "Collection Instance"
    bl_description = ("Instance a collection at cursor position")
    bl_icon = "ops.generic.select_circle"
    bl_keymap = (
        ("rgba.modal_instance_collection", {"type": 'LEFTMOUSE', "value": 'ANY'},
         {"properties": []}),
        )

    def draw_settings(context, layout, tool):
        """Drawing properties on screen"""
        props = tool.operator_properties("rgba.modal_instance_collection")
        layout.prop_search(context.scene.rgba, "collection_to_instance",
                           bpy.data,
                           "collections",
                           text="Collection to instance")
        layout.prop(props, "ignore_normal")
        layout.prop(props, "use_duplis")



class PreciseInstanceOperator(Operator):
    """Instance a collection at cursor position"""
    bl_idname = "rgba.modal_instance_collection"
    bl_label = "Instance Collection"
    bl_options = {'REGISTER', 'UNDO'}

    ignore_normal: BoolProperty(
        name="Ignore Surface Normal",
        default=False
    )

    use_duplis: BoolProperty(
        name="Use Duplis",
        default=False
    )

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def __init__(self):
        self.collection_name = None
        self.world_x = Vector((1.0, 0.0, 0.0))
        self.world_z = Vector((0.0, 0.0, 1.0))
        self.delta = 0
        self.impact_normal_mat = self.world_z
        self.impact_normal = None
        self.impact_location = None
        self.lmb = False
        self.surface_found = False
        self.help_string = ''
        self.mouse_path = None
        self.added_object = None
        self.surface_normal = None
        self.surface_hit = None
        self._handle = None
        self.heigh_modif = 1.0
        print("Precise Instance Started")

    def __del__(self):
        print("Precise Instance Ended")

    def execute(self, context):
        return {'FINISHED'}

    def modal(self, context, event):
        context.area.tag_redraw()
        region = context.region
        rv3d = context.space_data.region_3d

        if event.type == 'MOUSEMOVE':
            self.mouse_path = Vector((event.mouse_region_x, event.mouse_region_y))

            # get the ray from the viewport and mouse
            ray_result = get_ray_cast_result(
                context,
                self.mouse_path,
                self.use_duplis)

            self.surface_found = False
            if ray_result.hit is not None:
                self.surface_found = True
                self.surface_normal = ray_result.hit + ray_result.normal
                self.surface_hit = ray_result.hit

            if self.lmb: #if the left button is pressed
                root = view3d_utils.location_3d_to_region_2d(
                    region, rv3d, self.impact_location)
                mouse_distance = self.mouse_path-root  # 2D Vector from origin point to cursor

                #set the scale factor based on
                self.delta = mouse_distance.length
                if event.ctrl:
                    self.heigh_modif = self.delta * 0.003
                else:
                    self.scale_factor = self.delta * 0.003

                loc_mat = Matrix.Translation(self.added_object.location)
                scale_mat = Matrix.Scale(self.scale_factor, 4, (1, 0, 0)) @ \
                    Matrix.Scale(self.scale_factor, 4, (0, 1, 0)) @ \
                        Matrix.Scale(self.scale_factor*self.heigh_modif, 4, (0, 0, 1))

                x_aligned_vector = view3d_utils.location_3d_to_region_2d(
                    region, rv3d, self.world_x) - root
                x_aligned_vector = x_aligned_vector.normalized()

                mouse_distance = mouse_distance.normalized()

                rotation_factor = atan2(mouse_distance[1], mouse_distance[0]) - \
                     atan2(x_aligned_vector[1], x_aligned_vector[0])
                mat_rot = Matrix.Rotation(rotation_factor, 4, self.impact_normal_mat @ self.world_z)

                self.added_object.matrix_world = loc_mat @ mat_rot @ \
                    self.impact_normal_mat @ scale_mat

                self.help_string = str(rotation_factor)
                context.area.header_text_set(self.help_string)
        elif event.type == 'LEFTMOUSE' and event.value == 'RELEASE':
            self.lmb = False
            context.area.header_text_set('')
            SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'FINISHED'}
        elif event.type in {'RET', 'ESC'}:
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}
        if not context.scene.rgba.collection_to_instance:
            self.report({'WARNING'}, "Please select a collection to instance")
            return {'CANCELLED'}

        self.collection_name = context.scene.rgba.collection_to_instance
        self.lmb = True
        coord_mouse = Vector((event.mouse_region_x, event.mouse_region_y))
        # get the ray from the viewport and mouse
        ray = get_ray_cast_result(context, coord_mouse, self.use_duplis)
        if ray.hit is not None:
            self.world_x = ray.hit + Vector((1.0, 0.0, 0.0))
            self.impact_location = ray.hit
            self.impact_normal = ray.hit + ray.normal
            self.delta = 0

            self.added_object = add_prop_instance(context, self.collection_name)
            set_prop_loc_rot(self.added_object, ray.hit, ray.normal,scale=0.1, ignore_normal=self.ignore_normal)
            self.impact_normal_mat = self.added_object.rotation_euler.to_matrix().to_4x4()
        else:
            self.report({'WARNING'}, "No Object found !")
            return {'CANCELLED'}

        args = (self, context)
        self._handle = SpaceView3D.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_VIEW')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

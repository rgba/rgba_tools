""" Drawing functions """

import bpy
import bgl
import gpu
from gpu_extras.batch import batch_for_shader

def draw_callback_px(self, context):
    shader = gpu.shader.from_builtin('3D_UNIFORM_COLOR')
    bgl.glEnable(bgl.GL_BLEND)
    bgl.glLineWidth(2)
    batch = batch_for_shader(shader, 'LINES', {"pos": [self.impact_location,
                                                       self.impact_normal,
                                                       self.surface_hit,
                                                       self.impact_location]})
    shader.bind()
    shader.uniform_float("color", (1.0, 1.0, 0.0, 1))
    batch.draw(shader)

    # restore opengl defaults
    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)

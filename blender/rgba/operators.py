""" RGBa Tools operators """

from os import mkdir
from os.path import join, exists
import bpy
from bpy.types import Operator
from .utils import get_full_path, \
                   get_addon_prefs, \
                   dump_to_file, \
                   get_particle_system_info, \
                   get_export_format


def get_all_objects_in_collection(result, collection):
    """ Get a list of all objects in a collection, including in children collection"""
    for child in collection.children:
        get_all_objects_in_collection(result, child)
    for obj in collection.objects:
        result.append(obj)


def get_objects_statuses(objects):
    """ Store objects statuses visibility """
    result = {}
    for obj in objects:
        result[obj] = [obj.hide_select, obj.hide_viewport, obj.hide_render, obj.hide_get()]
    return result


def get_layer_collection(layer_collection, name):
    collection = None
    for child_layer in layer_collection.children:
        if child_layer.name == name:
            return child_layer
        else:
            collection = get_layer_collection(child_layer, name)
            if collection is not None:
                return collection


def get_all_collections_and_process_hide(objects, col, all_collections, collection_statuses):
    for col_name in col.children.keys():
        all_collections.append(col.children[col_name])
        layer_collection = get_layer_collection(bpy.context.view_layer.layer_collection, col_name)
        bpy.context.view_layer.active_layer_collection = layer_collection
        collection_statuses[col_name] = [col.children[col_name].hide_select,
                                            col.children[col_name].hide_viewport,
                                            col.children[col_name].hide_render,
                                            layer_collection.hide_viewport,
                                            layer_collection.exclude]

        if (col.children[col_name].hide_viewport or layer_collection.exclude or layer_collection.hide_viewport) and bpy.context.scene.rgba.ignore_hidden_collections:
            # remove objects in collection from objects list
            collection_objects = []
            get_all_objects_in_collection(collection_objects, col.children[col_name])
            for obj in collection_objects:
                objects.remove(obj)
        else:
            col.children[col_name].hide_select = False
            col.children[col_name].hide_viewport = False
            col.children[col_name].hide_render = False
            layer_collection.hide_viewport = False
            layer_collection.exclude = False
        get_all_collections_and_process_hide(objects, col.children[col_name], all_collections, collection_statuses)


#Methods
def export_selected_collection(self, context):
    """ Export the objects of the current collection to the defined project folder"""
    export_format = get_export_format(context)
    project_folder, project_cat, folder_name, file_name = get_full_path(
        context,
        trim_blend_file_number=context.scene.rgba.trim_blend_file_number,
        folder_use_collection_name=context.scene.rgba.folder_use_collection_name,
        file_use_collection_name=context.scene.rgba.file_use_collection_name,
        extension=export_format.lower())

    if not exists(join(project_folder, project_cat)):
        mkdir(join(project_folder, project_cat))
        print(f"The folder {project_cat} was created")

    if not exists(join(project_folder, project_cat, folder_name)):
        mkdir(join(project_folder, project_cat, folder_name))
        print(f"The folder {folder_name} was created")

    addon_prefs = get_addon_prefs(context)
    project = addon_prefs.export_project_collections.get(context.scene.rgba.export_project.name)

    export_path = join(project_folder, project_cat, folder_name, file_name)
    collection_to_export = context.scene.rgba.collection_to_export

    current_selection = context.selected_objects  # we store the current selection
    bpy.ops.object.select_all(action='DESELECT')

    props_prefix = project.props_prefix

    col = context.scene.collection.children[collection_to_export]
    objects = []
    get_all_objects_in_collection(objects, col)

    all_collections = []
    collection_statuses = {}
    get_all_collections_and_process_hide(objects, col, all_collections, collection_statuses)
    print("All collections: " + str(all_collections))
    print("All collection statuses: " + str(collection_statuses))

    statuses = get_objects_statuses(objects)
    for obj in objects:
        if (obj.hide_viewport or obj.hide_get()) and context.scene.rgba.ignore_hidden_objects:
            obj.select_set(False)
        else:
            # set visible to make obj selectable
            obj.hide_select = False
            obj.hide_viewport = False
            obj.hide_render = False
            obj.hide_set(False)
            obj.select_set(True)
        if obj.type == 'EMPTY' and obj.name.startswith(props_prefix):
            obj.instance_type = 'NONE'

    print("Export collextion \"" + collection_to_export + "\", with objects: " + str(context.selected_objects))

    if export_format == 'GLB':
        bpy.ops.export_scene.gltf(export_format='GLB',
                                  ui_tab='GENERAL',
                                  export_copyright="RGBa",
                                  export_image_format='AUTO',
                                  export_texture_dir="",
                                  export_texcoords=True,
                                  export_normals=True,
                                  export_draco_mesh_compression_enable=False,
                                  export_tangents=False,
                                  export_materials='EXPORT',
                                  export_colors=True,
                                  export_cameras=False,
                                  use_selection=True,
                                  export_extras=False,
                                  export_yup=True,
                                  export_apply=True,
                                  export_animations=True,
                                  export_frame_range=True,
                                  export_frame_step=1,
                                  export_force_sampling=True,
                                  export_nla_strips=True,
                                  export_def_bones=False,
                                  export_current_frame=False,
                                  export_skins=True,
                                  export_all_influences=False,
                                  export_morph=False,
                                  export_lights=False,
                                  export_displacement=False,
                                  will_save_settings=False,
                                  filepath=export_path,
                                  check_existing=False)
    elif export_format == 'GLTF':
        bpy.ops.export_scene.gltf(export_format='GLTF_SEPARATE',
                                  ui_tab='GENERAL',
                                  export_copyright="RGBa",
                                  export_image_format='AUTO',
                                  export_texture_dir="",
                                  export_texcoords=True,
                                  export_normals=True,
                                  export_draco_mesh_compression_enable=False,
                                  export_tangents=False,
                                  export_materials='EXPORT',
                                  export_colors=True,
                                  export_cameras=False,
                                  use_selection=True,
                                  export_extras=False,
                                  export_yup=True,
                                  export_apply=True,
                                  export_animations=True,
                                  export_frame_range=True,
                                  export_frame_step=1,
                                  export_force_sampling=True,
                                  export_nla_strips=True,
                                  export_def_bones=False,
                                  export_current_frame=False,
                                  export_skins=True,
                                  export_all_influences=False,
                                  export_morph=False,
                                  export_lights=False,
                                  export_displacement=False,
                                  will_save_settings=False,
                                  filepath=export_path,
                                  check_existing=False)
    elif export_format == 'FBX':
        bpy.ops.export_scene.fbx(filepath=export_path,
                                 bake_space_transform=True,
                                 use_selection=True,
                                 use_armature_deform_only=True,
                                 bake_anim_use_all_actions=False,
                                 check_existing=False)

    # We restore instances dupligroup
    for obj in objects:
        if obj.type == 'EMPTY' and obj.name.startswith(props_prefix):
            obj.instance_type = 'COLLECTION'

    # we restore objects default selection,visibility and render
    for obj in objects:
        obj.hide_select = statuses[obj][0]
        obj.hide_viewport = statuses[obj][1]
        obj.hide_render = statuses[obj][2]
        obj.hide_set(statuses[obj][3])

    # we restore collections default selection,visibility and render
    for col in all_collections:
        layer_collection = get_layer_collection(context.view_layer.layer_collection, col.name)

        col.hide_select = collection_statuses[col.name][0]
        col.hide_viewport = collection_statuses[col.name][1]
        col.hide_render = collection_statuses[col.name][2]
        layer_collection.hide_viewport = collection_statuses[col.name][3]
        layer_collection.exclude = collection_statuses[col.name][4]

    bpy.ops.object.select_all(action='DESELECT')
    for obj in current_selection:
        obj.select_set(True)


def generate_collection_json(self, context):
    """Generate a json file to the export collection"""
    project_folder, project_cat, folder_name, file_name = get_full_path(
        context,
        trim_blend_file_number=context.scene.rgba.trim_blend_file_number,
        folder_use_collection_name=context.scene.rgba.folder_use_collection_name,
        file_use_collection_name=context.scene.rgba.file_use_collection_name,
        extension='json')

    if not exists(join(project_folder, project_cat)):
        mkdir(join(project_folder, project_cat))
        print(f"The folder {project_cat} was created")

    if not exists(join(project_folder, project_cat, folder_name)):
        mkdir(join(project_folder, project_cat, folder_name))
        print(f"The folder {folder_name} was created")

    export_path = join(project_folder, project_cat, folder_name, file_name)

    collection_to_export = context.scene.rgba.collection_to_export
    col = context.scene.collection.children[collection_to_export]

    objects = []
    particules = []
    get_all_objects_in_collection(objects, col)
    degp = bpy.context.evaluated_depsgraph_get()
    for obj in objects:
        print(obj)
        if obj.type != 'MESH':
            continue
        for particle_system in obj.evaluated_get(degp).particle_systems:
            particules.append(get_particle_system_info(particle_system))

    dump_to_file(export_path, particules)


class ExportCollectionOperator(Operator):
    """Export the chosen collection to project folder"""
    bl_idname = "rgba.collection_to_export"
    bl_label = "Export Collection"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        export_selected_collection(self, context)
        return {'FINISHED'}

class GenerateJSONOperator(Operator):
    """Generate a json file to the export collection"""
    bl_idname = "rgba.generate_json"
    bl_label = "Generate JSON"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        generate_collection_json(self, context)
        return {'FINISHED'}

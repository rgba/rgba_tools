""" Paint Brush Tool """

import random
import bpy
from bpy.types import Operator, WorkSpaceTool, SpaceView3D, UILayout
from bpy.props import BoolProperty, FloatProperty
from mathutils import Vector
from .utils import add_prop_instance, get_ray_cast_result, \
    set_prop_loc_rot
from .draw import draw_callback_px


class PaintBrushTool(WorkSpaceTool):
    """ Instance Objects under cursor """
    bl_space_type = 'VIEW_3D'
    bl_context_mode = 'OBJECT'
    bl_idname = "rgba.paint_brush"
    bl_label = "Paint Instances"
    bl_description = ("Instance objects similar to poly brush in unity")
    bl_icon = "brush.paint_texture.draw"
    bl_keymap = (
        ("rgba.modal_paint_brush", {"type": 'LEFTMOUSE', "value": 'ANY'},
         {"properties": []}),
        )

    def draw_settings(context, layout : UILayout, tool):
        """Drawing properties on screen"""
        props = tool.operator_properties("rgba.modal_paint_brush")
        layout.prop_search(context.scene.rgba, "collection_to_instance",
                           bpy.data,
                           "collections",
                           text="Collection to instance")
        layout.prop(props, "brush_distance")
        layout.prop(props, "ignore_normal")
        layout.prop(props, "use_duplis")
        layout.prop(props, "use_random_scale")
        layout.prop(props, "min_scale")
        layout.prop(props, "max_scale")
        layout.prop(props, "use_random_rotation")
        layout.prop(props, "max_rotation")


class PaintBrushOperator(Operator):
    """ Instance Objects under cursor """
    bl_idname = "rgba.modal_paint_brush"
    bl_label = "Paint Instances"
    bl_options = {'REGISTER', 'UNDO'}

    brush_distance: FloatProperty(
        name="Brush Distance",
        default=1.0,
    )

    ignore_normal: BoolProperty(
        name="Ignore Surface Normal",
        default=False
    )

    use_duplis: BoolProperty(
        name="Use Duplis",
        default=False
    )

    use_random_scale: BoolProperty(
        name="Use Random scale",
        default=False
    )

    min_scale: FloatProperty(
        name="Min scale",
        default=0.1
    )

    max_scale: FloatProperty(
        name="Max scale",
        default=1
    )

    use_random_rotation: BoolProperty(
        name="Use Random rotation",
        default=False
    )

    max_rotation: FloatProperty(
        name="Max Rotation",
        default=3.14,
        subtype='ANGLE'
    )


    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def __init__(self):
        self.collection_name = None
        self.world_x = Vector((1.0, 0.0, 0.0))
        self.world_z = Vector((0.0, 0.0, 1.0))
        self.delta = 0
        self.impact_normal_mat = self.world_z
        self.impact_normal = None
        self.impact_location = None
        self.lmb = False
        self.surface_found = False
        self.help_string = ''
        self.mouse_path = None
        self.added_object = None
        self.surface_normal = None
        self.surface_hit = None
        self.previous_impact = None
        self._handle = None
        print("Paint Brush Started")

    def __del__(self):
        print("Paint Brush Ended")

    def execute(self, context):
        return {'FINISHED'}

    def modal(self, context, event):
        context.area.tag_redraw()
        if event.type == 'MOUSEMOVE':
            self.mouse_path = Vector((event.mouse_region_x, event.mouse_region_y))

            # get the ray from the viewport and mouse
            ray_result = get_ray_cast_result(
                context,
                self.mouse_path,
                self.use_duplis)

            self.surface_found = False
            if ray_result.hit is not None:
                self.surface_found = True
                self.surface_normal = ray_result.hit + ray_result.normal
                self.surface_hit = ray_result.hit

            if self.lmb: #if the left button is pressed
                self.delta += (self.previous_impact - self.surface_hit).length

                #add a object if the distance between the previous one is too short
                if self.delta > self.brush_distance:
                    prop = add_prop_instance(context, self.collection_name)
                    scale = 1.0
                    if self.use_random_scale:
                        scale = random.uniform(self.min_scale, self.max_scale)
                    rotation = 0.0
                    if self.use_random_rotation:
                        rotation = random.uniform(-self.max_rotation, self.max_rotation)
                    set_prop_loc_rot(prop, ray_result.hit, ray_result.normal,
                                     scale=scale,
                                     ignore_normal=self.ignore_normal,
                                     custom_rotation=rotation)
                    self.delta = 0.0
                    self.previous_impact = self.surface_hit

                context.area.header_text_set(self.help_string)
        elif event.type == 'LEFTMOUSE' and event.value == 'RELEASE':
            self.lmb = False
            context.area.header_text_set('')
            # SpaceView3D.draw_handler_remove(self._handle, 'WINDOW')
            return {'FINISHED'}
        elif event.type in {'RET', 'ESC'}:
            return {'CANCELLED'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found, cannot run operator")
            return {'CANCELLED'}
        if not context.scene.rgba.collection_to_instance:
            self.report({'WARNING'}, "Please select a collection to instance")
            return {'CANCELLED'}

        self.collection_name = context.scene.rgba.collection_to_instance
        self.lmb = True
        coord_mouse = Vector((event.mouse_region_x, event.mouse_region_y))
        # get the ray from the viewport and mouse
        ray = get_ray_cast_result(context, coord_mouse, self.use_duplis)
        if ray.hit is not None:
            self.world_x = ray.hit + Vector((1.0, 0.0, 0.0))
            self.impact_location = ray.hit
            self.impact_normal = ray.hit + ray.normal
            self.delta = 0

            self.added_object = add_prop_instance(context, self.collection_name)
            set_prop_loc_rot(self.added_object, ray.hit, ray.normal, ignore_normal=self.ignore_normal)
            self.impact_normal_mat = self.added_object.rotation_euler.to_matrix().to_4x4()
            self.previous_impact = ray.hit
        else:
            self.report({'WARNING'}, "No Object found !")
            return {'CANCELLED'}

        args = (self, context)
        # self._handle = SpaceView3D.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_VIEW')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

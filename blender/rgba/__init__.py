# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

""" Addon initialisation """

import bpy
from bpy.utils import register_class, register_tool, unregister_class, unregister_tool
from bpy.types import Operator, AddonPreferences, Panel, PropertyGroup, UIList
from bpy.props import CollectionProperty, StringProperty, IntProperty, PointerProperty, EnumProperty, BoolProperty
from .ui import ExportToolPanel, ExportToolPanel_Options, ExportToolPanel_Extra, InstanceCollectionPanel, RgbaVariousOperatorPanel
from .operators import ExportCollectionOperator, GenerateJSONOperator
from .precise_instance_tool import PreciseInstanceOperator, \
     PreciseInstanceTool
from .paint_brush_tool import PaintBrushOperator, PaintBrushTool
from .random_color_assign import RandomColorOperator
from .mesh_baker import MeshBakerOperator
from .utils import get_addon_prefs

bl_info = {
    "name": "RGBa Tools",
    "author": "Henri Hebeisen, Duy Kevin Nguyen, RGBa",
    "version": (1, 0),
    "blender": (3, 2, 0),
    "location": "View3D >T Menu",
    "description": "Various tools to help production of RGBa's project",
    "warning": "",
    "doc_url": "",
    "category": "Object",
}


class Project_OT_NewItem(Operator):
    """Add a new item to the list."""

    bl_idname = "project.new_item"
    bl_label = "Add a project item"

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        addon_prefs.export_project_collections.add()
        
        addon_prefs.project_index = len(addon_prefs.export_project_collections) - 1
        return{'FINISHED'}


class Project_OT_DeleteItem(Operator):
    """Delete the selected item from the list."""

    bl_idname = "project.delete_item"
    bl_label = "Deletes a project item"

    @classmethod
    def poll(cls, context):
        addon_prefs = get_addon_prefs(context)
        return addon_prefs.export_project_collections

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        collection_list = addon_prefs.export_project_collections
        index = addon_prefs.project_index

        collection_list.remove(index)
        addon_prefs.project_index = min(max(0, index - 1), len(collection_list) - 1)

        return{'FINISHED'}


class Project_OT_MoveItem(Operator):
    """Move an item in the project list."""

    bl_idname = "project.move_item"
    bl_label = "Move an item in the list"

    direction: EnumProperty(items=(('UP', 'Up', ""),
                                  ('DOWN', 'Down', ""),))

    @classmethod
    def poll(cls, context):
        addon_prefs = get_addon_prefs(context)
        return len(addon_prefs.export_project_collections) > 1

    def move_index(self, context):
        """ Move index of an item render queue while clamping it. """
        addon_prefs = get_addon_prefs(context)
        index = addon_prefs.project_index
        list_length = len(addon_prefs.export_project_collections) - 1  # (index starts at 0)
        new_index = index + (-1 if self.direction == 'UP' else 1)

        addon_prefs.project_index = max(0, min(new_index, list_length))

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        my_list = addon_prefs.export_project_collections
        index = addon_prefs.project_index

        neighbor = index + (-1 if self.direction == 'UP' else 1)
        my_list.move(neighbor, index)
        self.move_index(context)
        addon_prefs.project_index = neighbor

        return{'FINISHED'}


class Project_UL_List(UIList):
    """Demo UIList."""
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # We could write some code to decide which icon to use here...
        custom_icon = 'OBJECT_DATAMODE'

        # Make sure your code supports all 3 layout types
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            layout.label(text=item.name, icon = custom_icon)
        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon = custom_icon)


class Project(bpy.types.PropertyGroup):
    name: StringProperty(name="Project Name", default="")

    folder_path: StringProperty(
        name="Project source folder",
        default='',
        subtype='DIR_PATH'
    )

    export_format: EnumProperty(
        name="Export format",
        items=[('GLB', 'GLB', 'GLB', 'GLB', 0),
            ('GLTF', 'GLTF', 'GLTF', 'GLTF', 1),
            ('FBX', 'FBX', 'FBX', 'FBX', 2)
        ],
        default='GLB'
    )

    props_prefix: StringProperty(
        name="Props instance prefix",
        default='p_'
    )

    multimesh_prefix: StringProperty(
        name="Multimesh instance prefix",
        default='m_'
    )


class Category_OT_NewItem(Operator):
    """Add a new item to the list."""

    bl_idname = "category.new_item"
    bl_label = "Add a category item"

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        addon_prefs.categories.add()
        addon_prefs.category_index = len(addon_prefs.categories) - 1
        return{'FINISHED'}


class Category_OT_DeleteItem(Operator):
    """Delete the selected item from the list."""

    bl_idname = "category.delete_item"
    bl_label = "Deletes a category item"

    @classmethod
    def poll(cls, context):
        addon_prefs = get_addon_prefs(context)
        return addon_prefs.categories

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        collection_list = addon_prefs.categories
        index = addon_prefs.category_index

        collection_list.remove(index)
        addon_prefs.category_index = min(max(0, index - 1), len(collection_list) - 1)

        return{'FINISHED'}


class Category_OT_MoveItem(Operator):
    """Move an item in the category list."""

    bl_idname = "category.move_item"
    bl_label = "Move an item in the list"

    direction: EnumProperty(items=(('UP', 'Up', ""),
                                  ('DOWN', 'Down', ""),))

    @classmethod
    def poll(cls, context):
        addon_prefs = get_addon_prefs(context)
        return len(addon_prefs.categories) > 1

    def move_index(self, context):
        """ Move index of an item render queue while clamping it. """
        addon_prefs = get_addon_prefs(context)
        index = addon_prefs.category_index
        list_length = len(addon_prefs.categories) - 1  # (index starts at 0)
        new_index = index + (-1 if self.direction == 'UP' else 1)

        addon_prefs.category_index = max(0, min(new_index, list_length))

    def execute(self, context):
        addon_prefs = get_addon_prefs(context)
        my_list = addon_prefs.categories
        index = addon_prefs.category_index

        neighbor = index + (-1 if self.direction == 'UP' else 1)
        my_list.move(neighbor, index)
        self.move_index(context)

        return{'FINISHED'}


class Category_UL_List(UIList):
    """Category UIList."""
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # We could write some code to decide which icon to use here...
        custom_icon = 'OBJECT_DATAMODE'

        # Make sure your code supports all 3 layout types
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            layout.label(text=item.name, icon = custom_icon)
        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon = custom_icon)


class Category(bpy.types.PropertyGroup):
    name: StringProperty(name="Category Name", default="")


class RGBaAddonPreferences(AddonPreferences):
    """RGBa Addon Preferences"""
    bl_idname = __name__

    export_project_collections: CollectionProperty(type=Project)
    project_index: IntProperty(name="Project index", default=0)

    random_color_value: StringProperty(
        name="Vertex Color Layer Name",
        default='Random_Col_Value'
    )

    def get_color_tint(self, context):
        """Get a list of color value scheme"""
        result = []
        result.append(('dark', 'Dark', ""))
        result.append(('light', 'Light', ""))
        return result

    categories: CollectionProperty(name="Categories", type=Category)
    category_index: IntProperty(name="Category index", default=0)

    color_tint: EnumProperty(items=get_color_tint)

    def draw(self : Panel, context):
        """Drawing layout UI"""
        layout = self.layout
        scene = context.scene

        # Projects
        row = layout.row()
        row.label(text="Projects")
        row_list = layout.row(align=True)
        col_list = row_list.column(align=True)
        col_list.operator('project.new_item', icon="FILE", text="")
        col_list.separator()
        col_list.operator('project.move_item', icon="TRIA_UP", text="").direction = 'UP'
        col_list.operator('project.move_item', icon="TRIA_DOWN", text="").direction = 'DOWN'
        row_list.separator()
        row_list.template_list("Project_UL_List", "name", self,
                               "export_project_collections", self, "project_index")
        row_list.separator()
        row_list.operator('project.delete_item', icon="X", text="")

        if self.project_index >= 0 and self.export_project_collections:
            item = self.export_project_collections[self.project_index]

            col = layout.column()
            col.prop(item, "name")
            col.prop(item, "folder_path")
            col.prop(item, "export_format")
            col.prop(item, "props_prefix")
            col.prop(item, "multimesh_prefix")

        # Categories
        row = layout.row()
        row.label(text="Categories")
        row_list = layout.row(align=True)
        col_list = row_list.column(align=True)
        col_list.operator('category.new_item', icon="FILE", text="")
        col_list.separator()
        col_list.operator('category.move_item', icon="TRIA_UP", text="").direction = 'UP'
        col_list.operator('category.move_item', icon="TRIA_DOWN", text="").direction = 'DOWN'
        row_list.separator()
        row_list.template_list("Category_UL_List", "name", self,
                               "categories", self, "category_index")
        row_list.separator()
        row_list.operator('category.delete_item', icon="X", text="")

        if self.category_index >= 0 and self.categories:
            item = self.categories[self.category_index]

            col = layout.column()
            col.prop(item, "name")

class RGBaAddonProperties(PropertyGroup):
    """"Store properties in the active scene"""
    export_project: PointerProperty(type=Project)
    export_category: PointerProperty(type=Category)
    collection_to_export: StringProperty()
    collection_to_instance: StringProperty()
    ignore_hidden_objects: BoolProperty(
        name="Ignore Hidden Objects",
        default=False
    )
    ignore_hidden_collections: BoolProperty(
        name="Ignore Hidden Collections",
        default=False
    )
    trim_blend_file_number: BoolProperty(
        name="Trim Blend file to remove version number",
        default=True
    )
    folder_use_collection_name: BoolProperty(
        name="Use Collection Name As Folder Name",
        default=False
    )
    file_use_collection_name: BoolProperty(
        name="Use Collection Name As File Name",
        default=True
    )
    baker_destination: StringProperty()

classes = (
    Project,
    Project_UL_List,
    Project_OT_NewItem,
    Project_OT_DeleteItem,
    Project_OT_MoveItem,
    Category,
    Category_UL_List,
    Category_OT_NewItem,
    Category_OT_DeleteItem,
    Category_OT_MoveItem,
    RGBaAddonPreferences,
    RGBaAddonProperties,
    ExportToolPanel,
    ExportToolPanel_Options,
    ExportToolPanel_Extra,
    InstanceCollectionPanel,
    RgbaVariousOperatorPanel,
    ExportCollectionOperator,
    PreciseInstanceOperator,
    PaintBrushOperator,
    GenerateJSONOperator,
    RandomColorOperator,
    MeshBakerOperator,
)

tools = (
    PreciseInstanceTool,
    PaintBrushTool
)

# Registration
def register():
    """Register all tools, ui and operators"""
    for cls in classes:
        register_class(cls)

    for tool in tools:
        register_tool(tool)
    bpy.types.Scene.rgba = bpy.props.PointerProperty(type=RGBaAddonProperties)

def unregister():
    """Unregister all tools, ui and operators"""
    for cls in classes:
        unregister_class(cls)
    for tool in tools:
        unregister_tool(tool)
    del bpy.types.Scene.rgba

if __name__ == "__main__":
    register()

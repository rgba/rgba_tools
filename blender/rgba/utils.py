""" Utilities class """

import json
from json import JSONEncoder
import bpy
from bpy_extras import view3d_utils
from mathutils import Matrix, Vector, Quaternion
from bpy.types import Collection


class RGBaEncoder(JSONEncoder):
    """Custom encoder for Blender Types"""
    def default(self, o):
        """Check type"""
        if isinstance(o, Vector):
            return o[:]
        elif isinstance(o, Quaternion):
            return o[:]
        elif isinstance(o, Collection):
            return o.name
        else:
            return o.__dict__


def dump_to_file(file_path, data):
    """Save a json file at given location"""
    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, cls=RGBaEncoder, sort_keys=False, indent=1)
def get_addon_prefs(context):
    """ Return the addon preferences """
    preferences = context.preferences
    return preferences.addons[__package__].preferences


def get_full_path(context, trim_blend_file_number=True, folder_use_collection_name=False, file_use_collection_name=False, extension='glb'):
    """ Return folder path, categories, folder name and file name"""
    addon_prefs = get_addon_prefs(context)
    project = addon_prefs.export_project_collections.get(context.scene.rgba.export_project.name)

    project_folder = bpy.path.abspath(project.folder_path)
    project_cat = context.scene.rgba.export_category.name
    base_name = bpy.path.basename(bpy.context.blend_data.filepath)[:-6]
    if trim_blend_file_number:
        last_underscore = base_name.rfind("_")
        base_name = base_name[:last_underscore]
    collection_name = context.scene.rgba.collection_to_export

    if folder_use_collection_name:
        folder_name = collection_name
    else:
        folder_name = base_name
    file_name = f"{collection_name if file_use_collection_name else base_name}.{extension}"

    return project_folder, project_cat, folder_name, file_name

def get_export_format(context):
    """ Return folder path, categories, folder name and file name"""
    addon_prefs = get_addon_prefs(context)
    project = addon_prefs.export_project_collections.get(context.scene.rgba.export_project.name)
    return project.export_format


class RayCastResult():
    """ A simple object storing best ray cast result"""
    def __init__(self, hit, obj, normal, face_index):
        self.hit = hit
        self.object = obj
        self.normal = normal
        self.face_index = face_index

def add_prop_instance(context, collection_name):
    """Add a new empty to the scene with a dupligroup on it.
       We assume that the group is already linked to scene!"""

    addon_prefs = get_addon_prefs(context)
    project = addon_prefs.export_project_collections.get(context.scene.rgba.export_project.name)

    col = bpy.data.collections[collection_name]

    if collection_name.startswith(project.props_prefix):
        new_obj_name = f'{collection_name}_'
    else:
        new_obj_name = f'{project.props_prefix}{collection_name}_'
    new_obj = bpy.data.objects.new(new_obj_name, None)
    new_obj.instance_type = 'COLLECTION'
    new_obj.instance_collection = col
    new_obj.empty_display_size = 1
    new_obj.empty_display_type = 'PLAIN_AXES'
    context.scene.collection.objects.link(new_obj)

    return new_obj

def set_prop_loc_rot(prop, location, rotation, scale=1.0, ignore_normal=False, custom_rotation=0.0):
    """Place the given prop according to loc and rot parameters"""
    world_z = Vector((0.0, 0.0, 1.0))
    loc_mat = Matrix.Translation(location)

    scale_mat = Matrix.Scale(scale, 4, (1, 0, 0)) @ \
        Matrix.Scale(scale, 4, (0, 1, 0)) @ \
        Matrix.Scale(scale, 4, (0, 0, 1))

    if ignore_normal:
        orig_rot_mat = world_z.rotation_difference(world_z).to_matrix().to_4x4()
    else:
        orig_rot_mat = world_z.rotation_difference(rotation).to_matrix().to_4x4()

    mat_rot = Matrix.Rotation(custom_rotation, 4, rotation)
    prop.matrix_world = loc_mat @ mat_rot @ orig_rot_mat @ scale_mat

def obj_ray_cast(obj, matrix, ray_origin, ray_target):
    """Wrapper for ray casting that moves the ray into object space"""

    # get the ray relative to the object
    matrix_inv = matrix.inverted()
    ray_origin_obj = matrix_inv @ ray_origin
    ray_target_obj = matrix_inv @ ray_target
    ray_direction_obj = ray_target_obj - ray_origin_obj

    # cast the ray
    success, location, normal, face_index = obj.ray_cast(ray_origin_obj, ray_direction_obj)

    if success:
        return location, normal, face_index
    else:
        return None, None, None

def visibles_objects_without_builder_props(context):
    """Loop over (object, matrix) pairs (mesh only)"""
    for obj in context.visible_objects:
        if obj.type == 'MESH':
            if obj.parent is not None:
                print(obj.parent.name)
            yield (obj, obj.matrix_world.copy())


def visible_objects_and_duplis(context):
    """Loop over (object, matrix) pairs (mesh only)"""

    depsgraph = context.evaluated_depsgraph_get()
    for dup in depsgraph.object_instances:
        if dup.is_instance:  # Real dupli instance
            obj = dup.instance_object
            yield (obj, dup.matrix_world.copy())
        else:  # Usual object
            obj = dup.object
            yield (obj, obj.matrix_world.copy())


def get_ray_cast_result(context, coord_mouse, use_all_objects=False):
    """ Get the position location and normal from a ray cast at mouse position """
    region = context.region
    rv3d = context.space_data.region_3d
    # get the ray from the viewport and mouse
    view_vector = view3d_utils.region_2d_to_vector_3d(region, rv3d, coord_mouse)
    ray_origin = view3d_utils.region_2d_to_origin_3d(region, rv3d, coord_mouse)

    ray_target = ray_origin + view_vector

    # cast rays and find the closest object
    best_length_squared = -1.0
    best_obj = None
    best_hit = None #best hit location
    best_normal = None
    best_face_index = None

    obj_list = None
    if use_all_objects:
        obj_list = visible_objects_and_duplis(context)
    else:
        obj_list = visibles_objects_without_builder_props(context)

    for obj, matrix in obj_list:
        if obj.type != 'MESH':
            continue
        hit, normal, face_index = obj_ray_cast(obj, matrix, ray_origin, ray_target)
        if hit is not None:
            hit_world = matrix @ hit
            length_squared = (hit_world - ray_origin).length_squared
            if best_obj is None or length_squared < best_length_squared:
                best_length_squared = length_squared
                best_obj = obj
                best_hit = hit_world

                rot_quaternion = matrix.decompose()[1]
                best_normal = rot_quaternion.to_matrix() @ normal
                best_face_index = face_index

    return RayCastResult(best_hit, best_obj, best_normal, best_face_index)

def delete_temp_object(obj_to_delete):
    """ Remove object from scene """
    bpy.data.objects.remove(obj_to_delete, do_unlink=True)


def get_particle_system_info(particle_system):
    """Return a dictionary containing particle system and all
    particles position and rotation"""
    system = {}
    system['name'] = particle_system.settings.name
    system['mesh_collection'] = particle_system.settings.instance_collection
    system['scale'] = particle_system.settings.particle_size
    system['scale_random'] = particle_system.settings.size_random
    system['particles'] = []

    for part in particle_system.particles:
        pa_dict = {}
        pa_dict['location'] = swizzle_yup_location(part.location)
        pa_dict['rotation'] = swizzle_yup_rotation(part.rotation)
        system['particles'].append(pa_dict)

    # for part in particle_system.child_particles:
    #     pa_dict = {}
    #     pa_dict['location'] = part.location
    #     pa_dict['rotation'] = part.rotation
    #     system['particles'].append(pa_dict)

    return system


def swizzle_yup_location(loc: Vector) -> Vector:
    """Manage Yup location."""
    return Vector((loc[0], loc[2], -loc[1]))


def swizzle_yup_rotation(rot: Quaternion) -> Quaternion:
    """Manage Yup rotation."""
    return Quaternion((rot[0], rot[1], rot[3], -rot[2]))

def is_instance_object(obj):
    '''Check if an object is a collection instance'''
    return obj.type == 'EMPTY' and obj.instance_type == 'COLLECTION'

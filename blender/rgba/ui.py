# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

""" UI Panels for Rgba Tool addon """

from os import stat
from os.path import join, exists
from datetime import datetime
import bpy
from bpy.types import Panel
from .utils import get_full_path, get_addon_prefs, get_export_format


class ExportToolPanel(Panel):
    """Export a whole collection to project as asset or scene"""
    bl_label = "Export Tool"
    bl_category = "RGBa"
    bl_idname = "RGBA_PT_export_tool"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context = "objectmode"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        addon_prefs = get_addon_prefs(context)

        layout.prop_search(scene.rgba.export_project, "name",
                           addon_prefs,
                           "export_project_collections",
                           text="Project folder")

        layout.prop_search(scene.rgba, "collection_to_export",
                           bpy.data,
                           "collections",
                           text="Collection to export")

        row = layout.row(align=True)
        layout.prop_search(scene.rgba.export_category, "name",
                           addon_prefs,
                           "categories",
                           text="Category")

        if not scene.rgba.export_project.get("name"):
            row = layout.row(align=True)
            row.label(text="Please select a Project.",
                      icon="FILE_BLEND")
            return
            
        project_folder, project_cat, folder_name, file_name = get_full_path(
            context,
            trim_blend_file_number=scene.rgba.trim_blend_file_number,
            folder_use_collection_name=scene.rgba.folder_use_collection_name,
            file_use_collection_name=scene.rgba.file_use_collection_name,
            extension=get_export_format(context).lower())

        if not bpy.data.is_saved:
            row.label(text="Please save the file before exporting it.",
                      icon="FILE_BLEND")
            return
        
        if not project_folder:
            row.label(text="Please setup the project folder in the addon preferences.",
                      icon="ERROR")
            return

        row = layout.row(align=True)
        row.operator("rgba.collection_to_export", icon="EXPORT")
        row = layout.row(align=True)
        full_path = join(project_folder, project_cat, folder_name, file_name)
        if exists(full_path):
            info = stat(full_path)
            row.label(text=f"File {full_path} already exists and will overwritten !",
                        icon="INFO")
            row = layout.row(align=True)
            row.label(text=f"File was last modified on : \
                {datetime.fromtimestamp(info.st_mtime).strftime('%Y-%m-%d-%H:%M')}")
        else:
            row.label(text=f"File will be saved at {full_path}.")


class ExportToolPanel_Options(Panel):
    """Export options"""
    bl_label = "Export Options"
    bl_parent_id = "RGBA_PT_export_tool"
    bl_category = "RGBa"
    bl_idname = "RGBA_PT_export_options"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context = "objectmode"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        layout.prop(scene.rgba, "ignore_hidden_objects")
        layout.prop(scene.rgba, "ignore_hidden_collections")
        layout.prop(scene.rgba, "trim_blend_file_number")
        layout.prop(scene.rgba, "folder_use_collection_name")
        layout.prop(scene.rgba, "file_use_collection_name")


class ExportToolPanel_Extra(Panel):
    """Export extra stuff"""
    bl_label = "Export Extra"
    bl_parent_id = "RGBA_PT_export_tool"
    bl_category = "RGBa"
    bl_idname = "RGBA_PT_export_extra"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context = "objectmode"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        if not scene.rgba.export_project.get("name"):
            row = layout.row(align=True)
            row.label(text="Please select a Project.",
                      icon="FILE_BLEND")
            return

        project_folder, project_cat, folder_name, file_name = get_full_path(
            context,
            trim_blend_file_number=scene.rgba.trim_blend_file_number,
            folder_use_collection_name=scene.rgba.folder_use_collection_name,
            file_use_collection_name=scene.rgba.file_use_collection_name,
            extension=get_export_format(context).lower())

        if not bpy.data.is_saved:
            row = layout.row(align=True)
            row.label(text="Please save the file before exporting it.",
                      icon="FILE_BLEND")
            return

        if not project_folder:
            row = layout.row(align=True)
            row.label(text="Please setup the project folder in the addon preferences.",
                      icon="ERROR")
            return
        
        row = layout.row(align=True)
        row.label(text="Export Particules")
        row = layout.row(align=True)
        row.operator("rgba.generate_json", icon="FILE")


class InstanceCollectionPanel(Panel):
    """Instance a collection at cursor position"""
    bl_label = "Precise Instance Collection"
    bl_category = "RGBa"
    bl_idname = "RGBA_PT_instance_collection"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context = "objectmode"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        scene = context.scene


        layout.prop_search(scene.rgba, "collection_to_instance",
                           bpy.data,
                           "collections",
                           text="Collection to instance")

        row = layout.row(align=True)
        row.operator("rgba.modal_instance_collection", icon="BRUSH_CLONE")


class RgbaVariousOperatorPanel(Panel):
    """Here we display all the operators which don't have there own panel"""
    bl_label = "Tools"
    bl_category = "RGBa"
    bl_idname = "RGBA_PT_various_operators"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_context = "objectmode"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        addon_prefs = get_addon_prefs(context)
        layout = self.layout
        scene = context.scene

        layout.prop(addon_prefs, "color_tint", text="Color Tint")
        row = layout.row(align=True)
        row.operator("rgba.assign_random_colors", icon="BRUSH_CLONE")
        row = layout.row(align=True)
        row.prop_search(scene.rgba, "baker_destination",
                    bpy.data,
                    "collections",
                    text="Collection Destination")
        row = layout.row(align=True)
        row.operator('rgba.mesh_baker')

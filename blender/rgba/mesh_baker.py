"""Bake the current selection into one single mesh"""
import bpy
from bpy.types import Operator
from .utils import is_instance_object

def mesh_baker(self, context):
    '''Import the current selection and bake into a single mesh'''
    if not context.scene.rgba.baker_destination:
        self.report({'WARNING'}, "Please choose a collection to send the baked mesh ! ")
        return

    temp_collection = bpy.data.collections.new("temp_collection")
    context.scene.collection.children.link(temp_collection)

    target_collection = bpy.data.collections[context.scene.rgba.baker_destination]
    for obj in context.selected_objects:
        new_obj = obj.copy()
        if obj.type != 'EMPTY':
            new_obj.data = obj.data.copy()
        new_obj.animation_data_clear()
        temp_collection.objects.link(new_obj)

    instances = [obj for obj in temp_collection.objects if is_instance_object(obj)]
    meshes = [obj for obj in temp_collection.objects if obj.type == 'MESH']
    bpy.ops.object.select_all(action='DESELECT')
    for instance in instances:
        instance.select_set(True)
        bpy.ops.object.duplicates_make_real()
        instance.select_set(False)

    #we filter out objects that are not mesh:
    for obj in temp_collection.objects:
        if obj.type == 'EMPTY':
            bpy.data.objects.remove(obj, do_unlink=True)

    #we make each mesh real. This is quite long
    for obj in temp_collection.objects:
        context.view_layer.objects.active = obj
        obj.select_set(True)
        bpy.ops.object.convert(target='MESH')
        bpy.ops.object.select_all(action='DESELECT')

    #clean up collection as Blender keeps a copy of each linked mesh
    for obj in temp_collection.objects:
        if obj.data.library is not None:
            bpy.data.objects.remove(obj, do_unlink=True)


    geometry, collider = [], []
    for obj in temp_collection.objects:
        geometry.append(obj) if '-colonly' in obj.name else collider.append(obj)

    # we select and merge geometry meshes
    for obj in geometry:
        obj.select_set(True)
    context.view_layer.objects.active = geometry[0]
    bpy.ops.object.join()

    bpy.ops.object.select_all(action='DESELECT')

    # we select and merge collider meshes
    for obj in collider:
        obj.select_set(True)
    context.view_layer.objects.active = collider[0]
    bpy.ops.object.join()

    merged_geo = temp_collection.objects[0]
    merged_col = temp_collection.objects[1]
    target_collection.objects.link(merged_geo)
    target_collection.objects.link(merged_col)

    # we remove the temp collection
    temp_collection.objects.unlink(merged_geo)
    temp_collection.objects.unlink(merged_col)
    context.scene.collection.children.unlink(temp_collection)
    bpy.data.collections.remove(temp_collection)


class MeshBakerOperator(Operator):
    """Operator wrapper"""
    bl_idname = "rgba.mesh_baker"
    bl_label = "Bake into single mesh"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        mesh_baker(self, context)
        return {'FINISHED'}

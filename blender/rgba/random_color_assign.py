"""Assign the current selected objects with a random [0,1] value per vertex island"""
from random import uniform
import bmesh
from bpy.types import Operator
from .utils import get_addon_prefs

def walk_island(vert):
    ''' walk all un-tagged linked verts '''
    vert.tag = True
    yield(vert)
    linked_verts = [e.other_vert(vert) for e in vert.link_edges
                    if not e.other_vert(vert).tag]

    for v in linked_verts:
        if v.tag:
            continue
        yield from walk_island(v)

def get_islands(mesh, verts):
    '''return all islands'''
    def tag(verts, switch):
        for v in verts:
            v.tag = switch
    tag(mesh.verts, True)
    tag(verts, False)
    ret = {"islands" : []}
    verts = set(verts)
    while verts:
        v = verts.pop()
        verts.add(v)
        island = set(walk_island(v))
        ret["islands"].append(list(island))
        tag(island, False) # remove tag = True
        verts -= island
    return ret

def assign_random_colors(self, context):
    """Assign random colors"""
    objs = context.selected_objects
    for obj in objs:
        if obj.type != 'MESH':
            self.report({'WARNING'}, "Please select a mesh object")
            return
        mesh = obj.data
        bm = bmesh.new()
        bm.from_mesh(mesh)
        color_name = get_addon_prefs(context).random_color_value

        color_layer = bm.loops.layers.color.get(color_name)
        if color_layer is None:
            color_layer = bm.loops.layers.color.new(color_name)
        islands = [island for island in get_islands(bm, verts=bm.verts)["islands"]]
        vertex_colors = {}
        for i in islands:
            color_tint = get_addon_prefs(context).color_tint
            if color_tint == 'dark':
                random_value = uniform(0.1, 0.4)
            else:
                random_value = uniform(0.5, 1.0)
            random_color = [random_value for c in "rgb"] + [1]
            for vert in i:
                vertex_colors[vert] = random_color

        for face in bm.faces:
            for loop in face.loops:
                loop[color_layer] = vertex_colors[loop.vert]

        bm.to_mesh(mesh)
        bm.clear()

class RandomColorOperator(Operator):
    """Generate a json file to the export collection"""
    bl_idname = "rgba.assign_random_colors"
    bl_label = "Assign Random Colors"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        assign_random_colors(self, context)
        return {'FINISHED'}
